<?php
declare(strict_types = 1);
require_once "config.php";

if(!empty ($_POST)) {
    $login = $_POST['login'];
    $pass = $_POST['password'];
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    $stmt = $pdo->prepare("
        SELECT
        `id`,
        `name`
    FROM
        `users` 
    WHERE 
        `name` = :name and `password` = :password
"
    );

    //$stmt->execute([$login, $pass]);
    $stmt->execute(["name" => $login, "password" => $pass]);
    $result = $stmt->fetch();
    if (!empty($result)) {
        echo "you are logged!";
    } else {
        echo "login or password is invalid!";
    }
}
?>
<html>
<body>
<?php if (!empty($_GET['error'])): ?>
        <h3>login or password is invalid!</h3>
<?php endif; ?>
<form action="sql_injections.php" method="post">
    <p>
        <label>LOGIN</label>
        <input type="text" name="login" value="">
    </p>
    <p>
        <label>PASSWORD</label>
        <input type="password" name="password" value="">
    </p>
    <input type="submit" value="Sign In">
    </form>
</body>
</html>

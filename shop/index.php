<?php
require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "config.php";
require_once FUNCTIONS_PATH . "db.php";
$_SESSION['user_id'] = 1;
$products = json_decode(file_get_contents(ROOT_PATH . DIRECTORY_SEPARATOR . "shop" . DIRECTORY_SEPARATOR . 'products.json'), true);

require_once ROOT_PATH . DIRECTORY_SEPARATOR . "tamplates" . DIRECTORY_SEPARATOR . "products.php";
/*$products = [
    ["name" => "Apple iPhone 11 Pro Max", "price" => 50000, "quantity" => 15, 'category' => "Apple"],
    ["name" => "Apple Watch Series 6 GPS", "price" => 20000, "quantity" => 21, 'category' => "Apple"],
    ["name" => "Samsung Galaxy S20", "price" => 50000, "quantity" => 17, 'category' => "Samsung"],
    ["name" => "Samsung Galaxy Watch 3", "price" => 50000, "quantity" => 19, 'category' => "Samsung"],
    ["name" => "Xiaomi Mi 11 Pro", "price" => 50000, "quantity" => 23, 'category' => "Xiaomi"],
    ["name" => "Xiaomi Mi Smart Band 4 NFC", "price" => 50000, "quantity" => 14, 'category' => "Xiaomi"]
];
file_put_contents(__DIR__ . DIRECTORY_SEPARATOR . 'products.json', json_encode($products));
die();*/

$products = getAllProducts($pdo);


if (!empty($_POST['products'])) {
    if(!empty($_SESSION['cart_id'])){
        clearCart($pdo, $_SESSION['cart_id']);
    } else {
        $_SESSION['cart_id'] = createCart($pdo, $_SESSION['user_id']);
    }
    foreach($_POST['products'] as $product) {
        addProductToCart($pdo, $_SESSION['cart_id'], (int) $product);
    } die();
    header("location: /shop/cart.php");
    die();
}




